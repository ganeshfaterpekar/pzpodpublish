#
# Be sure to run `pod lib lint Umbrella.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
#

Pod::Spec.new do |s|
s.name              = 'PZSWIFTSDK'
s.version           = '2.0.0'
s.summary           = 'Description of PZ Framework.'

s.description      = <<-DESC
A bigger description of PZ Framework.
DESC

s.homepage          = 'https://pointzi.com'
s.license           = { :type => 'Apache-2.0', :file => 'LICENSE.txt' }
s.authors           = { 'Author one' => 'ganeshfaterpekar@gmail.com'}
s.source            = { :http => 'http://pkg.streethawk.com/artifactory/pointzi-ios/streethawk/pointzi-sdk-ios-swift/buildfix/-beta+52684d9/Pointzi.zip'}

s.ios.deployment_target = '10.0'
s.ios.vendored_frameworks = 'Pointzi/PointziFramework.framework'

# Add all the dependencies
s.dependency 'Alamofire'

end
